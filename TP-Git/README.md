# TP GIT
## Manip' 1 : Partons d'un code existant (non-versionné)
### Initialisation du dépôt
1. Aller dans un répertoire contenant du code de votre choix
```shell
$ cd /path/to/local/repo
```
1. Initialiser un dépôt git
```shell
$ git init
```

### Utilisation du dépôt
* Effectuer des modifications
* Vérifier les statuts
* Suivre les modifications
* Un premier commit ?
## Manip' 2 : Clonons un projet existant sur gitlab
### Validation des accès à gitlab
* Sur l'interface web
* en SSH
#### Configurer une connexion SSH
* Se connecter au Meso@LR
* Créer une paire de clé privé/public
```shell
ssh-keygen -t rsa -b 4096
```
### Initialisation du dépôt
* Se déplacer dans le répertoire de destination locale
* On clone ou on fork ?
### Utilisation du dépôt local
* Effectuer des modifications
* Suivre les modifications
* Commit
* Enregistrer les modifications sur le dépôt distant
    * Nouvelle branche
    * Fusion de branche

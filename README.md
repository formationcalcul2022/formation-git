# formation-git
## Support de présentation
Le support de présentation est dans le répertoire presentation. Il est écrit en LaTeX en utilisant le template Beamer et le thème INRAE développé par ... entre autres Éric Quinton de Cestas-Gazinet.

## Travaux Pratiques
L'espace de TP est dans le répertoire TP-Git.

## Automatisation
À chaque commit, d'après le fichier de configuration de l'intégration continue, si un fichier .tex a été modifié, on lance une compilation pour générer un fichier PDF.

Le fichier généré est visible en tant qu'artefact du pipeline concerné.

Bonjour
